import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { Observable } from 'rxjs';
import { Producto } from '../models/product.model';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  private _http = inject(HttpClient);
  baseUrl: string = 'https://fakestoreapi.com';

  getProducts(): Observable<Producto[]> {
    return this._http.get<Producto[]>(`${this.baseUrl}/products`);
  }

  getProduct(id: number): Observable<Producto> {
    return this._http.get<Producto>(`${this.baseUrl}/products/${id}`);
  }
}
