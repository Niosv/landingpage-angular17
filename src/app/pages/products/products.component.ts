import { Component, OnInit, inject } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Producto } from '../../models/product.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-products',
  standalone: true,
  imports: [],
  templateUrl: './products.component.html',
  styleUrl: './products.component.css',
})
export class ProductsComponent implements OnInit {
  private _apiService = inject(ApiService);
  private _router = inject(Router);
  productos: Producto[] = [];

  ngOnInit(): void {
    this.getProducts();
    console.log(this.productos);
  }

  getProducts() {
    this._apiService.getProducts().subscribe(
      (data: Producto[]) => {
        this.productos = data;
      },
      (error) => {
        console.error('Error al cargar productos:', error);
      }
    );
  }

  navigate(id: number): void {
    this._router.navigate(['/products', id]);
  }
}
