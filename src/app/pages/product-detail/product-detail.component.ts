import { Component, OnInit, inject } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Producto } from '../../models/product.model';
import { ActivatedRoute } from '@angular/router';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-product-detail',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './product-detail.component.html',
  styleUrl: './product-detail.component.css',
})
export class ProductDetailComponent implements OnInit {
  private _route = inject(ActivatedRoute);
  private _apiService = inject(ApiService);
  public product?: Producto;
  loading: boolean = true;

  ngOnInit(): void {
    this._route.params.subscribe((params) =>
      this._apiService.getProduct(params['id']).subscribe((data: Producto) => {
        this.product = data;
        this.loading = false;
      })
    );
  }

  getProduct() {}
}
